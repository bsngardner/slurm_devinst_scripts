#!/bin/bash

# full slurm mirror created with
# $ git clone --mirror <url>

if [ -z "${SLURM_GIT+set}" ]; then
	SLURM_GIT=$HOME/slurm/slurm.git
fi

echo "Looking for mirror in $SLURM_GIT"
cd $SLURM_GIT
rc=$?; if [[ $rc != 0 ]]; then
	echo "Not found"
	exit $rc
fi

git remote update --prune

echo "Finished mirror repo update"

