#!/bin/sh
INST_NAME="$1"

if [ -z "${INST_NAME+set}" ]; then
        echo "First arg must be instance name"
        exit 1
fi

PORTBASE="$2"

# This tests if PORTBASE is ""
if [ -z "${PORTBASE}" ]; then
        echo "Using default port base 10000"
        PORTBASE=10000
fi

bail() {
	echo "Missing variable $1"
	echo "Export necessary variables or define them in local_env.sh"
	exit 1
}

if [ -z "${USER+set}" ]; then
	bail USER
fi
if [ -z "${CLUSTER+set}" ]; then
	bail CLUSTER
fi
if [ -z "${INST_ROOT+set}" ]; then
	bail INST_ROOT
fi
if [ -z "${TEMPLATE_DIR+set}" ]; then
	bail TEMPLATE_DIR
fi
if [ -z "${SLURM_GIT+set}" ]; then
	bail SLURM_GIT
fi

printf "USER:\t\t$USER\n"
printf "CLUSTER:\t$CLUSTER\n"
printf "INST_ROOT:\t$INST_ROOT\n"
printf "TEMPLATE_DIR:\t$TEMPLATE_DIR\n"
printf "SLURM_GIT:\t$SLURM_GIT\n"

# Edit if you want
NODENAME=node
NODECOUNT=16

HOME=/home/$USER
INST_DIR=$INST_ROOT/$INST_NAME
PREFIX=$INST_DIR/$CLUSTER
BUILD_DIR=$PREFIX/build
SPOOL_DIR=$PREFIX/spool
LOG_DIR=$PREFIX/log
STATE_DIR=$PREFIX/state
CHECKPOINT_DIR=$STATE_DIR/checkpoint
RUN_DIR=$PREFIX/run
ETC_DIR=$PREFIX/etc
LIB_DIR=$PREFIX/lib
SRC_DIR=$INST_DIR/source

SLURMCTLD_PORT="$PORTBASE"
SLURMDBD_PORT=$(($PORTBASE + 1))
SLURMD_PORT=$(($PORTBASE + 2))
NODESTARTPORT=$(($PORTBASE + 100))
NODELASTPORT=$(($NODESTARTPORT + $NODECOUNT - 1))
NODERANGE=$NODENAME[0-$(($NODECOUNT - 1))]

if [ -d "$INST_DIR" ]; then
        echo "A Slurm instance by that name already exists"
        exit
fi
echo "Making new Slurm instance: $INST_NAME"

echo "Making directories..."
echo $SRC_DIR && mkdir -p $SRC_DIR
echo $PREFIX && mkdir -p $PREFIX
echo $BUILD_DIR && mkdir -p $BUILD_DIR
echo $ETC_DIR && mkdir -p $ETC_DIR
echo $SPOOL_DIR && mkdir -p $SPOOL_DIR
echo $LOG_DIR && mkdir -p $LOG_DIR
echo $STATE_DIR && mkdir -p $STATE_DIR
echo $CHECKPOINT_DIR && mkdir -p $CHECKPOINT_DIR
echo $LIB_DIR && mkdir -p $LIB_DIR
echo $RUN_DIR && mkdir -p $RUN_DIR

echo "Cloning slurm repo"
git clone -l $SLURM_GIT $SRC_DIR

echo "Copying templates over"
cp -r $TEMPLATE_DIR/etc $PREFIX/
cp -r $TEMPLATE_DIR/envrc $INST_DIR/.envrc

echo "$INST_DIR/.envrc"
#sed -i "/CLUSTER=/c\CLUSTER=$CLUSTER" $INST_DIR/.envrc # already defined
sed -i "/INST_NAME=/c\INST_NAME=$INST_NAME" $INST_DIR/.envrc
sed -i "/INST_DIR=/c\INST_DIR=$INST_DIR" $INST_DIR/.envrc
sed -i "/PORTBASE=/c\PORTBASE=$PORTBASE" $INST_DIR/.envrc
sed -i "/NODELIST=/c\NODELIST=$NODERANGE" $INST_DIR/.envrc
eval "direnv allow $INST_DIR/.envrc"

echo "Modifying conf files"
echo "$ETC_DIR/slurm.conf"
echo "Slurmctld port: $SLURMCTLD_PORT"
echo "Slurmd port: $SLURMD_PORT"
echo "Slurmdbd port: $SLURMDBD_PORT"
echo "Node port range: $NODESTARTPORT - $NODELASTPORT"
sed -i "/ControlMachine=CLUSTER/c\ControlMachine=$CLUSTER" $ETC_DIR/slurm.conf
sed -i "/JobCheckpointDir=/c\JobCheckpointDir=$CHECKPOINT_DIR" $ETC_DIR/slurm.conf
sed -i "/SlurmctldPidFile=/c\SlurmctldPidFile=$RUN_DIR/slurmctld.pid" $ETC_DIR/slurm.conf
sed -i "/SlurmctldPort=/c\SlurmctldPort=$SLURMCTLD_PORT" $ETC_DIR/slurm.conf
sed -i "/SlurmdPidFile=/c\SlurmdPidFile=$RUN_DIR/slurmd.%n.pid" $ETC_DIR/slurm.conf
sed -i "/SlurmdPort=/c\SlurmdPort=$SLURMD_PORT" $ETC_DIR/slurm.conf
sed -i "/SlurmdSpoolDir=/c\SlurmdSpoolDir=$SPOOL_DIR/slurmd.%n" $ETC_DIR/slurm.conf
sed -i "/SlurmUser=/c\SlurmUser=$USER" $ETC_DIR/slurm.conf
sed -i "/StateSaveLocation=/c\StateSaveLocation=$STATE_DIR" $ETC_DIR/slurm.conf
sed -i "/ClusterName=CLUSTER/c\ClusterName=$CLUSTER" $ETC_DIR/slurm.conf
sed -i "/SlurmctldLogFile=/c\SlurmctldLogFile=$LOG_DIR/slurmctld.log" $ETC_DIR/slurm.conf
sed -i "/SlurmdLogFile=/c\SlurmdLogFile=$LOG_DIR/slurmd.%n.log" $ETC_DIR/slurm.conf
sed -i "/AccountingStoragePort=/c\AccountingStoragePort=$SLURMDBD_PORT" $ETC_DIR/slurm.conf
sed -i "s/Port=nodeports/Port=$NODESTARTPORT-$NODELASTPORT/g" $ETC_DIR/slurm.conf
sed -i "s/NodeName=nodename/NodeName=$NODERANGE/g" $ETC_DIR/slurm.conf
sed -i "s/NodeHostname=hostname/NodeHostname=$CLUSTER/g" $ETC_DIR/slurm.conf
sed -i "s/Nodes=allnodes/Nodes=$NODERANGE/g" $ETC_DIR/slurm.conf

echo "$ETC_DIR/slurmdbd.conf"
sed -i "/LogFile=/c\LogFile=$LOG_DIR/slurmdbd.log" $ETC_DIR/slurmdbd.conf
sed -i "/PidFile=/c\PidFile=$RUN_DIR/slurmdbd.pid" $ETC_DIR/slurmdbd.conf
sed -i "/StorageLoc=/c\StorageLoc=slurm_$INST_NAME" $ETC_DIR/slurmdbd.conf
sed -i "/StorageUser=/c\StorageUser=$USER" $ETC_DIR/slurmdbd.conf
sed -i "/DbdPort=/c\DbdPort=$SLURMDBD_PORT" $ETC_DIR/slurmdbd.conf
sed -i "/DbdHost=/c\DbdHost=$CLUSTER" $ETC_DIR/slurmdbd.conf
sed -i "/SlurmUser=/c\SlurmUser=$USER" $ETC_DIR/slurmdbd.conf
sed -i "/StorageUser=/c\StorageUser=$USER" $ETC_DIR/slurmdbd.conf

if [ ! -z "${SLURM_MKSLURMINST_SCRIPT+set}" -a -f "$SLURM_MKSLURMINST_SCRIPT" ]; then
	"$SLURM_MKSLURMINST_SCRIPT" "$INST_DIR"
fi
