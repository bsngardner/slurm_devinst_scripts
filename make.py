#!/usr/bin/env python3

import argparse
import asyncio
import itertools as it
import logging
import os
import shlex
import signal
import subprocess
import sys
import time
from contextlib import contextmanager
from pathlib import Path


build = Path('.')
src = build/'src'
plugin_dirs = [src/'plugins'/x for x in (
    'mpi',
    'task',
)]
RECURSE = set((src, src/'plugins', *plugin_dirs))
#RECURSE = set((src, build/'contribs', src/'plugins', *plugin_dirs))
#RECURSE = set([src, src/'plugins'])
SKIP = set([build/'etc', build/'testsuite', build/'auxdir'])
made = set()


def rmdir(path):
    try:
        paths = iter(path)
        for p in paths:
            rmdir(p)
        return
    except TypeError:
        pass
    path = Path(path)
    if not path.exists() and not path.is_symlink():
        return
    if path.is_dir():
        rmdir(path.iterdir())
        path.rmdir()
    else:
        path.unlink()


def is_slurm_build_dir(path):
    path = Path(path)
    required = (
        path/'config.log',
        path/'config.status',
        path/'config.h',
    )
    one = (
        path/'slurm/slurm.h',
        path/'slurm/slurm_version.h'
    )
    return (
        all(p.exists() for p in required) and
        any(p.exists() for p in one)
    )


async def run_subproc(cmd, shell=False, cwd=Path('.'), **kwargs):
    global output
    kwargs['cwd'] = str(cwd.resolve())
    kwargs.update(output)
    if shell:
        process = await asyncio.create_subprocess_shell(cmd, **kwargs)
    else:
        args = shlex.split(cmd)
        process = await asyncio.create_subprocess_exec(*args, **kwargs)

    await process.wait()


async def make(path=Path('.')):
    global args
    if path in made:
        log.log(debug2, "already done: {}".format(path))
        return

    # if dir is trivially recursable, recurse
    if path in SKIP:
        return
    elif path in RECURSE:
        children = [x for x in path.iterdir() if x.is_dir()]
        await asyncio.gather(*map(make, children))
    elif (path/'Makefile').exists():
        made.add(path)
        cmd = "make -j install" if args.install else "make -j"

        log.info("make: {}".format(path))
        t0 = time.time()
        await run_subproc(cmd, cwd=path)
        t1 = time.time()

        log.log(debug2, "done in {:0.2f}s: {}".format(t1-t0, path))


async def configure(args):
    # Because this operation requires user input, output is not optional
    # Is that reasonable?
    print('Configuring build dir')
    print('Build dir: {}'.format(args.build_dir))
    print(args.configure)
    while True:
        print('Confirm Y/n')
        resp = input().lower()
        if resp in ('', 'y'):
            break
        elif resp == 'n':
            return False
    print('configuring')
    rmdir(args.build_dir.iterdir())
    await run_subproc(args.configure, shell=True)
    return True


async def clean():
    log.info('cleaning build directory')
    cmd = 'make -j clean'
    log.info('\t{}'.format(cmd))
    await run_subproc(cmd, shell=True)


async def recheck():
    log.info('running configure recheck')
    cmd = './config.status --recheck'
    log.info('\t{}'.format(cmd))
    await run_subproc(cmd, shell=True)


async def reconfig():
    log.info('running reconfigure')
    cmd = './config.status'
    log.info('\t{}'.format(cmd))
    await run_subproc(cmd, shell=True)


async def main(args):
    if args.init:
        await configure(args)
    else:
        if args.recheck:
            await recheck()
        if args.reconfig:
            await reconfig()
    if args.clean:
        await clean()

    # 'src/database' and 'src/bcast' are independent of 'src/common', 'src/api',
    # and 'src/db_api'
    first = asyncio.gather(*map(make, (src/'database', src/'bcast')))
    await make(src/'common')
    await make(src/'api')
    if (src/'db_api').exists():
        await make(src/'db_api')
    await asyncio.gather(*map(make, (src/'slurmd/common', src/'lua')))
    await first  # but these need to be done before continuing

    # extra dirs are specified in --extra option
    extra = [make(build/x) for x in args.extra]
    if args.docs:
        extra.append(make(build/'doc'))
    if args.contribs:
        extra.append(make(build/'contribs'))
    extra = asyncio.gather(*extra)
    await make(src)
    if args.install:
        await run_subproc('make install-pkgincludeHEADERS')
    await extra


log_level = {
    'critical': 0,
    'error': 1,
    'info': 2,
    'debug': 3,
}

debug2 = 60
logging.addLevelName(debug2, 'debug2')  # independent logging level
log_map = (
    logging.CRITICAL,
    logging.ERROR,
    logging.INFO,
    logging.DEBUG,
)

OPTIONS = (
    ('build_dir',
     dict(action='store', nargs='?', type=lambda s: Path(s), default="",
          help="specify the build directory. Current working directory is used by default. If current dir is not a slurm build dir and SLURM_PREFIX env var exists, uses that instead")),
    ('--init', '-i',
     dict(dest='init', action='store_true', default=[],
          help="initialize build dir using configure flags in CONFIG_FLAGS. Also enables --serial. Requires variables CONFIG_FLAGS and (SLURM_SRC or INST_DIR) to be defined")),
    ('--serial', '-s',
     dict(dest='serial', action='store_true',
          help="Disable parallel build, just run make from the top")),
    ('--no-install', '-n',
     dict(dest='install', action='store_false',
          help="build only, do not install")),
    ('--with-clean', '-c',
     dict(dest='clean', action='store_true',
          help="run make clean before make")),
    ('--with-reconfig', '-r',
     dict(dest='reconfig', action='count',
          help="run config.status before make. Pass a second time to also include --with-recheck. This allows -rr")),
    ('--with-recheck',
     dict(dest='recheck', action='store_true',
          help="run config.status --recheck before make.")),
    ('--with-all', '-a',
     dict(dest='all', action='store_true',
          help="make docs and contribs")),
    ('--with-docs',
     dict(dest='docs', action='store_true',
          help="also make docs")),
    ('--with-contribs',
     dict(dest='contribs', action='store_true',
          help="also make contribs")),
    ('--with-extra', '-e',
     dict(dest='extra', action='append', default=[],
          help="specify extra subdirs to build, such as contribs/pmi2. Use multiple times or pass comma-separated list")),
    ('--level',
     dict(dest='level', action='store', default='error', choices=log_level.keys(),
          help="Set log level. Default is error, critical disables make stderr")),
    ('-v', '--verbose',
     dict(dest='verbose', action='count', default=0,
          help="Increase log level by 1")),
    ('-q', '--quiet',
     dict(dest='quiet', action='count', default=0,
          help="Reduce log level by 1")),
    ('--stdout',
     dict(dest='stdout', action='store_true',
          help="output full make stdout like a reckless child")),
    ('--no-stderr',
     dict(dest='stderr', action='store_false',
          help="Force disable make stderr")),
    ('--debug2',
     dict(dest='debug2', action='store_true',
          help=argparse.SUPPRESS)),
)

parser = argparse.ArgumentParser(
    description="Fast make slurm. Does not make docs or contribs by default.")
for x in OPTIONS:
    parser.add_argument(*x[:-1], **x[-1])
args = parser.parse_args()

log = logging.getLogger(__name__)
args.verbosity = log_level[args.level]
args.verbosity = max(0, min(3, args.verbosity + args.verbose - args.quiet))

log.setLevel(log_map[args.verbosity])
out = logging.StreamHandler(sys.stdout)
out.addFilter(lambda r: r.levelno <= logging.INFO)
err = logging.StreamHandler(sys.stderr)
err.addFilter(lambda r:  logging.INFO < r.levelno <= logging.CRITICAL)
if args.debug2:
    dbg2 = logging.StreamHandler(sys.stdout)
    dbg2.addFilter(lambda r: r.levelno == debug2)
    log.addHandler(dbg2)
log.addHandler(out)
log.addHandler(err)

# For subprocess output, block stdout unless log level is DEBUG
output = dict(stdout=None, stderr=None)
if not args.stdout:
    output['stdout'] = subprocess.DEVNULL
if args.stderr:
    if log.level > logging.ERROR:
        output['stderr'] = subprocess.DEVNULL
else:
    output['stderr'] = subprocess.DEVNULL

if args.all:
    args.docs, args.contribs = True, True

# --with-recheck specified more than once turns on reconfig, allowing -rr
if args.reconfig:
    if args.reconfig > 1:
        args.recheck = True
    args.reconfig = True

if args.init:
    args.recheck = False
    args.reconfig = False
    args.clean = False
    if 'CONFIG_FLAGS' in os.environ:
        config_flags = os.environ['CONFIG_FLAGS']
    else:
        log.error("--init flag passed, but CONFIG_FLAGS env var not found")
        exit()
    if 'SLURM_SRC' in os.environ:
        srcdir = os.environ['SLURM_SRC']
    else:
        if 'INST_DIR' in os.environ:
            srcdir = Path(os.environ['INST_DIR'])/'source'
        else:
            log.error("--init flag passed, but SLURM_SRC or INST_DIR env var not found")
            exit()
    args.configure = f'{srcdir}/configure {config_flags}'

if not is_slurm_build_dir(args.build_dir):
    if 'SLURM_PREFIX' in os.environ:
        args.build_dir = Path(os.environ['SLURM_PREFIX'])/'build'
        log.error("Using SLURM_PREFIX: {}".format(os.environ['SLURM_PREFIX']))
    if not is_slurm_build_dir(args.build_dir) and not args.init:
        log.error("This does not appear to be a Slurm build directory. Make sure to run configure first.")
        exit()
# flatten list of lists of specific dirs
args.extra = list(it.chain(*[s.split(',') for s in args.extra]))
if args.serial:
    RECURSE = set()


@contextmanager
def cd(path):
    prev_dir = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_dir)


async def shutdown(loop, signal=None):
    if signal:
        print(f"Exit signal {signal.name}")
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    for t in tasks:
        t.cancel()
    await asyncio.gather(*tasks, return_exceptions=True)
    loop.stop()


def handle_exception(loop, context):
    msg = context.get('exception', context['message'])
    print(f"caught exception {msg}")
    asyncio.ensure_future(shutdown(loop))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.ensure_future(shutdown(loop, signal=s)))
    loop.set_exception_handler(handle_exception)

    with cd(args.build_dir):
        try:
            loop.run_until_complete(main(args))
        except asyncio.exceptions.CancelledError:
            print("caught CancelledError")
        except BrokenPipeError:
            print("caught BrokenPipeError")
        finally:
            loop.close()
        #asyncio.run(main(args))
