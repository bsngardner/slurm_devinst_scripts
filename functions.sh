#!/bin/sh

check_env() {
	if [ -z "${SLURM_PREFIX:-set}" ]; then
		env_set=false
		echo "No slurm env detected; SLURM_PREFIX is empty"
		return
	fi
	if [ -z "${NODELIST:-set}" ]; then
		env_set=false
		echo "Slurm env incomplete; missing NODELIST"
		return
	fi
	env_set=true
	SBIN=$SLURM_PREFIX/sbin
	BIN=$SLURM_PREFIX/bin
}

#test_func() {
#	check_env
#	if test "$env_set" = false; then return; fi
#	if test "$env_set" = true; then
#		echo good!
#	else
#		echo nope!
#	fi
#}

restart_slurmd() {
	check_env
	if test "$env_set" = false; then return; fi

        for node in `scontrol show hostnames=$NODELIST`
        do
		$(eval sudo $SBIN/slurmd -N "$node" &)
        done
}

restart_slurm() {
	check_env
	if test "$env_set" = false; then return; fi

	$(eval $SBIN/slurmdbd &)
        restart_slurmd
	$(eval $SBIN/slurmctld &)
	wait
}

clear_slurmdb() {
	check_env
	if test "$env_set" = false; then return; fi

	echo stopping slurmdbd
	stop_slurmdbd
	echo dropping database slurm_$NAME
        mysql -e "drop database slurm_$NAME;"
	eval $SBIN/slurmdbd
	sleep 1
}

init_slurmdb() {
	check_env
	if test "$env_set" = false; then return; fi

        echo "Making cluster and accounts"
        $BIN/sacctmgr -i add cluster $CLUSTER
        $BIN/sacctmgr -i add account dev
        $BIN/sacctmgr -i add account general
        $BIN/sacctmgr -i add user broderick account=dev
        $BIN/sacctmgr -i add user jarvis account=general
        $BIN/sacctmgr -i add user clippy account=general
}

#pidstop() {
#	if test ${1:-x} = "x"; then
#		return
#	fi
#
#	echo "$1" | while IFS= read -r line ; do
#		eval "xargs sudo kill $line"
#	done
#}

stop_slurmd() {
	check_env
	if test "$env_set" = false; then return; fi

    find $SLURM_PREFIX/run -name "slurmd.node.*.pid" \
        -exec bash -c 'echo `basename $0`' {} \; \
        -exec sudo pkill -F {} ';'
	#pidstop `sudo cat $SLURM_PREFIX/run/slurmd.node*(.N) 2>/dev/null`
}

stop_slurmdbd() {
	check_env
	if test "$env_set" = false; then return; fi

    find $SLURM_PREFIX/run -name "slurmdbd.pid" \
        -exec bash -c 'echo `basename $0`' {} \; \
        -exec pkill -F {} \;
	#pidstop `cat $SLURM_PREFIX/run/slurmdbd.pid 2>/dev/null`
}

stop_slurmctld() {
	check_env
	if test "$env_set" = false; then return; fi

    find $SLURM_PREFIX/run -name "slurmctld.pid" \
        -exec bash -c 'echo `basename $0`' {} \; \
        -exec pkill -F {} ';'
	#pidstop `cat $SLURM_PREFIX/run/slurmctld.pid 2>/dev/null`
}

stop_slurm() {
	check_env
	if test "$env_set" = false; then return; fi

    echo "Stopping slurm"
	$SLURM_PREFIX/bin/scontrol shutdown
	sleep 1
	stop_slurmdbd
	stop_slurmd
	stop_slurmctld
	wait
}

kill_slurm() {
	check_env
	if test "$env_set" = false; then return; fi

	echo "Force killing slurm"
	stop_slurm()
	sleep 1
    find $SLURM_PREFIX/run -name "slurm*.pid" \
        -exec bash -c 'echo `basename {}`' {} \; \
        -exec sudo pkill -KILL -F {} \; \
        -exec sudo rm -f {} \;
}

make_compiledb() {
	check_env
	if test "$env_set" = false; then return; fi

	if [ -z "${INST_DIR:-set}" ]; then
		echo INST_DIR not found, making from SLURM_PREFIX
		INST_DIR=$(dirname $SLURM_PREFIX)
	fi

	pushd $SLURM_PREFIX/build
	make -j clean > /dev/null
	bear -o $INST_DIR/compile_commands.json make -j install > /dev/null
	popd
}
